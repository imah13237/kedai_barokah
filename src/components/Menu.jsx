import React, {useState, useEffect} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const App = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [seleksi, setSeleksi] = useState();
  const Tab = createBottomTabNavigator();
  


  return (
    <View style={{flex: 1, backgroundColor: '#D8BFD8'}}>
       <TouchableOpacity
        style={{
          backgroundColor: '#778899',
          marginTop: 20,
          marginHorizontal: 20,
          paddingVertical: 10,
          borderRadius: 6,
          elevation: 2,
          
          paddingHorizontal: 20,
        }}
        onPress={() => setShowMenu(!showMenu)}>
      <View style={{marginHorizontal: 13}}>
      <Text style={{fontSize: 20, color: '#000000'}}>
      <Icon name="edit" size={20} color="#B0C4DE" />Menu Kedai Barokah</Text>
      </View>
      </TouchableOpacity>
      {showMenu && (
        <View
          style={{
            marginHorizontal: 20,
            backgroundColor: '#B0C4DE',
            elevation: 2,
            paddingVertical: 20,
            paddingHorizontal: 20,
            borderBottomRightRadius: 6,
            borderBottomLeftRadius: 6,
          }}>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 1 ? 2 : 0,
          borderColor: seleksi == 1 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(1)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 1 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Geprek</Text>
        </View>
      
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 2 ? 1 : 0,
          borderColor: seleksi == 2 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(2)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 2 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Geprek + Es Teh</Text>
        </View>
      
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 3 ? 2 : 0,
          borderColor: seleksi == 3 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(3)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 3 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Goreng</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 4 ? 3 : 0,
          borderColor: seleksi == 4 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(4)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 4 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Goreng + Es Teh</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 5 ? 4 : 0,
          borderColor: seleksi == 5 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(5)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 5 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Rames</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 6 ? 5 : 0,
          borderColor: seleksi == 6 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(6)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            borderColor: '#00b1fe',
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 6 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Goreng + Es Jeruk</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 7 ? 6 : 0,
          borderColor: seleksi == 7 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(7)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 7 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Nasi Pecel</Text>
        </View>
      
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          marginHorizontal: 20,
          marginTop: 20,
          backgroundColor: '#8FBC8F',
          elevation: 3,
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 6,
          flexDirection: 'row',
          borderWidth: seleksi == 8 ? 7 : 0,
          borderColor: seleksi == 8 ? '#00b1fe' : 'transparent',
        }}
        onPress={() => setSeleksi(8)}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderWidth: 3,
            width: 25,
            height: 25,
            borderRadius: 25 / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#00b1fe',
          }}>
          <View
            style={{
              backgroundColor: seleksi == 8 ? '#00b1fe' : '#FFFFFF',
              width: 10,
              height: 10,
              borderRadius: 10 / 2,
            }}></View>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Mie Goreng</Text>
        </View>
      
      </TouchableOpacity>
      
        </View>
      )}
    </View>
  );
};

export default App;